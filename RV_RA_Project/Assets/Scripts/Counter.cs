﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Counter : MonoBehaviour
{
    public Transform[] target;
    public Text frontWheelCanvas;
    public Text rightPadelCanvas;
    public Text leftPadelCanvas;
    public Text seatCanvas;
    public Text bikeCanvas;
    Rigidbody rb;
    public static bool semaforoWheel = false;
    public static bool semaforoLeftPadel = false;
    public static bool semaforoRightPadel = false;
    public static bool semaforoSeat = false;
    public static int contadorFrontWheel = 0;
    public static int contadorLeftPadel = 0;
    public static int contadorRightPadel = 0;
    public static int contadorSeat = 0;
    public static int contadorBicicletas = 0;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

        if (frontWheelCanvas != null && GameObject.FindGameObjectWithTag("FrontWheel") != null)
        {
            if (GameObject.FindGameObjectWithTag("FrontWheel").transform.position == target[0].position)
            {
                if (semaforoWheel == false)
                {
                    contadorFrontWheel++;
                    frontWheelCanvas.text = "Rodas: " + contadorFrontWheel;
                    semaforoWheel = true;
                }
            }
        }

        if (leftPadelCanvas != null && GameObject.FindGameObjectWithTag("LeftPadel"))
        {
            if (GameObject.FindGameObjectWithTag("LeftPadel").transform.position == target[1].position)
            {
                if (semaforoLeftPadel == false)
                {
                    contadorLeftPadel++;
                    leftPadelCanvas.text = "Pedais esquerdos: " + contadorLeftPadel;
                    semaforoLeftPadel = true;
                }
            }
        }

        if (rightPadelCanvas != null && GameObject.FindGameObjectWithTag("RightPadel"))
        {
            if (GameObject.FindGameObjectWithTag("RightPadel").transform.position == target[2].position)
            {
                if (semaforoRightPadel == false)
                {
                    contadorRightPadel++;
                    rightPadelCanvas.text = "Pedais direitos: " + contadorRightPadel;
                    semaforoRightPadel = true;
                }
            }
        }

        if (seatCanvas != null && GameObject.FindGameObjectWithTag("Seat"))
        {
            if (GameObject.FindGameObjectWithTag("Seat").transform.position == target[3].position)
            {
                if (semaforoSeat == false)
                {
                    contadorSeat++;
                    seatCanvas.text = "Bancos: " + contadorSeat;
                    semaforoSeat = true;
                }
            }
        }

        if ((frontWheelCanvas != null && GameObject.FindGameObjectWithTag("FrontWheel") != null)
            && (leftPadelCanvas != null && GameObject.FindGameObjectWithTag("LeftPadel") != null)
            && (rightPadelCanvas != null && GameObject.FindGameObjectWithTag("RightPadel") != null)
            && (seatCanvas != null && GameObject.FindGameObjectWithTag("Seat") != null))
        {
            if (GameObject.FindGameObjectWithTag("FrontWheel").transform.position == target[0].position
                && GameObject.FindGameObjectWithTag("LeftPadel").transform.position == target[1].position
                && GameObject.FindGameObjectWithTag("RightPadel").transform.position == target[2].position
                && GameObject.FindGameObjectWithTag("Seat").transform.position == target[3].position)
            {

                contadorBicicletas++;
                bikeCanvas.text = "Bicicletas: " + contadorBicicletas;

                Destroy(GameObject.FindGameObjectWithTag("FrontWheel"));
                Destroy(GameObject.FindGameObjectWithTag("LeftPadel"));
                Destroy(GameObject.FindGameObjectWithTag("RightPadel"));
                Destroy(GameObject.FindGameObjectWithTag("Seat"));

                semaforoWheel = false;
                semaforoLeftPadel = false;
                semaforoRightPadel = false;
                semaforoSeat = false;

            }
        }

    }
}
