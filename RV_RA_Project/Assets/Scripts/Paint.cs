﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paint : MonoBehaviour{
    public Transform[] target;
    public float speed;

    private int current;

    Rigidbody rb;

    private bool lastTarget = false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position != target[current].position)
        {
            if (current > 0 && current <= 2)
            {
                if (current == 1)
                {
                    GetComponent<Renderer>().material.color = Color.black;
                }

                if(lastTarget == false)
                {
                    Vector3 pos = Vector3.MoveTowards(transform.position, target[current].position, speed * Time.deltaTime);
                    GetComponent<Rigidbody>().MovePosition(pos);
                }
            }
        }
        else
        {
            if (current < target.Length - 1)
            {
                current = (current + 1) % target.Length;
            }
            else
            {
                lastTarget = true;
            }
        }
    }
}
